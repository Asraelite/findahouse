window.addEventListener('load', init);

function init() {
	Array.from(document.getElementsByClassName('reaction')).forEach(el => {
		el.addEventListener('click', async function() {
			let root = this.parentElement.parentElement.parentElement;
			let id = root.getAttribute('data-id');
			let response = await ajax('/react', {
				id: id,
				reaction: el.className.split('-')[1]
			});
			let main = root.querySelector('.reactions');
			let buttons = root.querySelectorAll('.buttons button');
			buttons.forEach(b => b.disabled = true);
			let nrel = root.querySelector('.no-reactions');
			if (nrel) nrel.parentElement.removeChild(nrel);
			let span = document.createElement('span');
			span.className = el.className;
			span.classList.remove('reaction');
			span.classList.add('new');
			let text = document.createTextNode(response);
			span.appendChild(text);
			main.appendChild(span);
			setTimeout(_ => span.classList.remove('new'), 50);
		});
	});
}

async function ajax(url, values) {
	return new Promise((resolve, reject) => {
		let req = new XMLHttpRequest();
		req.open('post', url, true);
			req.setRequestHeader('Content-type',
				'application/x-www-form-urlencoded');

		req.addEventListener('load', () => {
			req.status == 200 ? resolve(req.response) :
				reject(new Error(req.statusText));
		});

		req.addEventListener('error', () => {
			reject(new Error('Network error'));
		});

		let str = [];
		for (let i in values)
			str.push(i + '=' + encodeURIComponent(values[i]));
		req.send(str.join('&'));
	});
}
