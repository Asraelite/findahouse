const timeago = require('time-ago')().ago;
const sha256 = require('sha256');

module.exports = class Listing {
	constructor(data) {
		this.id = sha256(data.address || data.url || ('' + Math.random()));
		this.source = data.source;
		this.price = data.price || null;
		this.name = data.address || null
		this.beds = data.beds || null;
		this.ppprice = this.price / this.beds;
		this.expensive = this.ppprice > 750;
		this.bedTypes = data.bedTypes || {};
		this.rawDate = data.date;
		this.date = data.date.toGMTString() + ' (not exact)' ||
			null;
		this.old = +(Date.now() - data.date) > 400000000; // A few days
		this.dateAgo = timeago(this.date);
		if (data.date > Date.now()) this.dateAgo = 'today';
		this.distance = data.distance || null;
		this.furnishing = data.furnishing;
		//console.log(this.source, data.furnishing)
		this.url = data.url;
		this.images = data.images || [];
		this.iconUrl = this.images[0] || '/img/none.jpg';
		this.baths = data.baths || null;
		this.score = this.weigh();
		this.type = 'normal';

		{
			let arr = [];
			for (let i in this.bedTypes)
				arr.push(this.bedTypes[i] + ' ' + i);
			this.bedTypesString = arr.join(', ');
		}

		this.reactions = [];

		this.load(server.db);
	}

	load(db) {
		try {
			var data = db.getData('/listing/' + this.id);
		} catch (_) {
			return;
		}

		data.reactions.forEach(r => {
			this.react(server.users[r.user], r.reaction);
		});
		this.type = data.type;
		this.comments = data.comments;
	}

	save() {
		let data = {};

		data.reactions = this.reactions.map(r => {
			return {
				user: r.user.id,
				reaction: r.reaction
			};
		});

		data.type = this.type;
		data.comments = this.comments;

		server.db.push('/listing/' + this.id, data, true);
	}

	weigh() {
		let score = 400;
		score -= this.ppprice * 0.5;
		score += this.baths * 100;
		score -= this.distance * 10;
		if (this.furnishing) score += 100;
		return score | 0;
	}

	react(user, reaction) {
		if (reaction == 'viewed') this.type = 'viewed';

		this.reactions.push({
			user: user,
			reaction: reaction,
			time: Date.now()
		});

		if (this.reactions.length == 2 || reaction == 'veto') {
			this.evaluate();
		}
	}

	evaluate() {
		let reactions = {
			approve: 0,
			meh: 0,
			disapprove: 0,
			veto: 0
		};
		this.reactions.forEach(r => reactions[r.reaction]++);

		if (reactions.veto) {
			this.type = 'rejected';
		} else if (reactions.approve >= 1) {
			this.type = 'approved';
		} else {
			this.type = 'rejected';
		}
	}

	ratedBy(user) {
		return this.reactions.some(r => r.user.id == user.id);
	}
}
