const Listing = require('./listing');

module.exports = class Listings {
	constructor(server) {
		this.server = server;
		this.items = new Map();

		this.filters = {
			checkboxes: {
				rated: [false, 'rated'],
				rejected: [false, 'rejected'],
				viewed: [false, 'viewed'],
				threeBeds: [true, '3 beds'],
				unfurnished: [true, 'unfurnished'],
				expensive: [false, 'expensive af']
			},
			dropdowns: {
				'sort': ['score', 'Sort by:']
			}
		};
	}

	getJson(view, user) {
		let filters = {};
		for (let i in view.checkboxes) filters[i] = view.checkboxes[i][0];

	 	let arr = Array.from(this.items.values()).filter(item => {
			let f = filters;
			if (f.furnishing != null && !item.furnishing &&
				!view.unfurnished)
				return false;
			if (!f.rated && item.ratedBy(user)) return false;
			if (!f.threeBeds && item.beds == 3) return false;
			if (!f.rejected && item.type == 'rejected') return false;
			if (!f.viewed && item.type == 'viewed') return false;
			if (!f.expensive && item.expensive) return false;
			if (!f.old && item.old) return false;
			return true;
		});

		arr.sort((a, b) => {
			let s = view.dropdowns.sort[0];
			if (s == 'score')
				return b.score - a.score;
			if (s == 'price')
				return a.price - b.price;
			if (s == 'date')
				return b.rawDate - a.rawDate;
			if (s == 'proximity')
				return a.distance - b.distance;
		});

		return arr;
	}

	save() {
		this.items.forEach(l => l.save());
	}

	getAll() {
		return Array.from(this.items.values());
	}

	update(data) {
		if (this.getAll().some(v => v.url == data.url)) return;
		let listing = new Listing(data);
		this.items.set(listing.id, listing);
	}

	react(id, user, reaction) {
		if (!this.items.has(id)) return false;
		this.items.get(id).react(user, reaction);
	}
}
