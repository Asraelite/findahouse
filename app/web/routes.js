const express = require('express');
const bodyParser = require('body-parser');
const stylus = require('stylus');
const pug = require('pug');
const session = require('express-session');
const clone = require('clone');

module.exports = (webServer, app) => {
	let server = webServer.server;
	app.set('views', 'public/views');
	app.set('view engine', 'pug');
	app.engine('pug', pug.__express);
	app.use(bodyParser.urlencoded({
		extended: true
	}));

	app.use(stylus.middleware({
		src: 'public/stylus',
		dest: 'public/static/css',
		force: true
	}));

	app.use(session({
		secret: 'colinisajew',
		resave: false,
		saveUninitialized: true,
		cookie: {
			secure: false
		}
	}));

	app.use((req, res, next) => {
		req.json = {};
		req.json.users = server.users;
		next();
	});

	app.get('/login', (req, res) => {
		res.render('login', req.json);
	});

	app.get('/logout', (req, res) => {
		req.session.user = null;
		res.redirect('/login');
	});

	app.post('/login', (req, res) => {
		let value = Object.keys(req.body)[0];
		if (!(value in server.users)) {
			res.sendStatus(400);
		} else {
			req.session.user = server.users[value];
			res.redirect('/');
		}
	});

	app.use(express.static('public/static'));

	app.use((req, res, next) => {
		if (!req.session.view)
			req.session.view = clone(server.listings.filters);
		if (!req.session.user) {
			res.redirect('/login');
		} else {
			req.json.user = req.session.user;
			next();
		}
	});

	// Only add new routes below here.

	app.post('/react', (req, res) => {
		let value = req.body.reaction;
		server.listings.react(req.body.id, req.session.user, value);
		res.send(req.session.user.nick);
	});

	app.post('/filter', (req, res) => {
		let view = clone(server.listings.filters);
		for (let i in view.checkboxes) {
			view.checkboxes[i][0] = false;
			if (i in req.body)
				view.checkboxes[i][0] = true;
		}
		for (let i in view.dropdowns)
			if (i in req.body) view.dropdowns[i][0] = req.body[i];
		req.session.view = view;
		res.redirect('/');
	});

	app.get('/', (req, res) => {
		let lpp = 20;
		let listings = server.listings.getJson(req.session.view || {},
			req.session.user);
		let page = +req.query.page - 1 || 0;
		req.json.listings = listings.slice(page * lpp, (page + 1) * lpp);
		req.json.curPage = page + 1;
		req.json.pageCount = Math.ceil(listings.length / lpp);
		if (req.query.onePage) {
			req.json.listings = listings;
			req.json.pageCount = 1;
		}
		req.json.maxListings = server.listings.items.size;
		req.json.totalListings = listings.length;
		req.json.view = req.session.view;
		res.render('home', req.json);
	});
};
