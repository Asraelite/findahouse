const express = require('express');
require('colors');

const routes = require('./routes');

module.exports = class WebServer {
	constructor(server) {
		this.server = server;

		this.app = express();
	}

	start() {
		routes(this, this.app);

		let port = 6969;
		this.app.listen(port);
		console.log(('Server started on port ' + ('' + port).green).bold);
	}
}
