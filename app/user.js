module.exports = class User {
	constructor(id) {
		this.id = id;
		this.name = {
			cao: 'Caoimhe',
			col: '(((Colin)))',
			mar: 'Markus',
			ter: 'Teresa'
		}[this.id];
		this.nick = {
			cao: 'Ca',
			col: 'Co',
			mar: 'M',
			ter: 'T'
		}[this.id] || '?';
	}
}
