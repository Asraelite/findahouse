const cheerio = require('cheerio');
const request = require('request-promise-native');

const time = require('../utils/time');

module.exports = async params => {
	const maxPrice = 3500;
	const minBed = 3;
	const maxBed = 5;
	const minBath = 2;
	const maxAge = params.maxAge || 10;
	//const pageLimit = maxAge * 3;
	const pageLimit = 6;

	let boxes = [];

	let searchString = `http://www.rent.ie/houses-to-let/renting_dublin/` +
		`rent_0-${maxPrice}/sort-by_date-entered_down/`;

	async function countListings(callback) {
		let $ = cheerio.load(await request(searchString));
		return +$('#num_properties').text().replace(',', '');
	}

	async function scanPage(n) {
		let $ = cheerio.load(await request(searchString + '/page_' + n));
		let promises = [];
		$('.search_result').each((_, elem) => {
			promises.push(parseBox($, elem));
		});
		await Promise.all(promises);
	}

	async function parseBox($, elemObj) {
		let elem = $(elemObj);
		let box = {};

		let href = $('.sresult_description > div > a', elem).attr('href');
		box.url = href;

		let priceStr = $('.sresult_description > h4', elem).text().trim();
		let price = +priceStr.split(' ')[0].split('€')[1].replace(/,/g, '');
		if (priceStr[2] == 'weekly') price *= 4;
		box.price = price;

		let dateStr = $('.sresult_available_from', elem).text();
		let date = time.parse(dateStr.split('Entered')[1].trim());
		box.date = date;
		let timeDiff = Math.abs(Date.now() - date.getTime());
		let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		if (diffDays > maxAge) return;

		let desc = $('.sresult_description > h3', elem).text()
			.trim();
		let bedTypes = desc.match(/\(.+\)/);
		if (bedTypes) bedTypes = bedTypes[0];
		desc = desc.replace(/\(.+\)/, '').split(',');
		if (desc.length == 1) return;

		box.furnishing = desc.length > 2 ? !!desc[2].match(/\bfurnished/) :
			null;
		box.beds = +desc[0].trim().split(' ')[0];
		box.baths = +desc[1].trim().split(' ')[0];
		if (box.beds < minBed || box.beds > maxBed) return;
		if (box.baths < minBath) return;

		box.images = [$('.sresult_image_container img', elem).attr('src')];
		box.address = $('h2 > a', elem).text().trim();
		/*
		try {
			var $ = cheerio.load(await request(href));
		} catch (err) {
			throw err;
		}

		infoStr = $('#map_information .map_info_box').text().split(':');
		box.address = infoStr[1].split('Distance to')[0].trim();
		let match = box.address.match(/Dublin \d{1,2}W?/);
		if (match)
			match = 'D' + (match[0].split(' ')[1].length == 1 ? '0' : '') +
				match[0].split(' ')[1];
		box.areaCode = match || 'unknown';
		box.distance = +infoStr[2].split('km')[0].trim();

		box.images = $('.pbxl_carousel_item > img').map((_, el) => {
			return $(el).attr('src');
		}).get();

		let overviewStr = $('#overview ul').text().trim().split('\n');
		if (overviewStr.length < 2) {
			box.furnished = 'unknown';
			box.bedTypes = 'unknown';
		} else {
			box.furnishing = overviewStr[0] == 'Furnished';
			let bedTypes = overviewStr[1].split('(')[1].split(')')[0];
			box.bedTypes = {};
			bedTypes.split(',').forEach(b => {
				box.bedTypes[b.trim().split(' ')[1]] = +b.trim().split('')[0];
			});
		}

		box.facilities = [];
		$('#facilities li').each((_, f) =>box.facilities.push($(f).text()));

		box.url = $('.description_extras > a:nth-of-type(1)').attr('href');
		*/

		box.source = 'rent';

		boxes.push(box);
	}

	function align(string, n) {
		return string + ' '.repeat(Math.max(n - ('' + string).length, 0));
	}

	let count = await countListings();

	let promises = [];
	for (let i = 0; i < count / 20 && i < pageLimit; i++) {
		promises.push(scanPage(i));
	}
	await Promise.all(promises);
	return boxes;
};
