const daft = require('./daft.js');
const rent = require('./rent.js');

module.exports = class Scrapers {
	constructor(server) {
		this.server = server;
	}

	async run(params) {
		let websites = [daft(params)]//, rent(params)];
		let listings = [].concat.apply([], await Promise.all(websites));
		this.server.listings.items.clear();
		listings.forEach(l => this.server.listings.update(l));
	}
}
