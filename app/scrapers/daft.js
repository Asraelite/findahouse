const cheerio = require('cheerio');
const request = require('request-promise-native');

module.exports = async params => {
	const minPrice = 400;
	const maxPrice = 2500;
	const minBed = 2;
	const maxBed = 3;
	const minBath = 1;
	const maxAge = params.maxAge || 7;
	const limit = 20;

	let boxes = [];
	let requestCount = 0;

	let searchString = `http://www.daft.ie/dublin-city/residential-property-f` +
		`or-rent/?s%5Bmnp%5D=${minPrice}&s%5Bmxp%5D=${maxPrice}` +
		`&s%5Bmnb%5D=${minBed}&s%5Bmxb%5D=${maxBed}&s%5Bmnbt%5D` +
		`=${minBath}&s%5Bmin_lease%5D=9&s%5Bmax_lease%5D=12&s%5Badvanc` +
		`ed%5D=1&searchSource=rental&s%5Bsort_type%5D=d&s%5Bsort_by%5D=date`;

	async function countListings(callback) {
		let $ = cheerio.load(await request(searchString));
		return +$('.section strong:nth-of-type(1)').text().split(' ')[1];
	}

	async function scanPage(n) {
		let $ = cheerio.load(await request(searchString + '&offset=' + n * 20));
		let promises = [];
		$('.box').each((_, elem) => {
			promises.push(tryParseBox($, elem));
		});
		await Promise.all(promises);
	}

	async function tryParseBox($, elem) {
		try {
			return await parseBox($, elem);
		} catch (err) {
			console.log('Failed to parse Daft box');
			return false;
		}
	}

	async function parseBox($, elemObj) {
		let elem = $(elemObj);
		let box = {};

		let href = $('h2 a', elem).attr('href');

		let priceStr = $('.price', elem).text().split(' ');
		let price = +priceStr[0].split('€')[1].replace(/,/g, '');
		if (priceStr[2] == 'week') price *= 4;
		box.price = price;

		let infoStr = $('.info', elem);
		box.type = infoStr.find('li:nth-of-type(1)').text().trim().split(' ')[0];
		box.beds = +infoStr.find('li:nth-of-type(2)').text().trim().split(' ')[0];
		box.baths = +infoStr.find('li:nth-of-type(3)').text().trim().split(' ')[0];

		let dateStr = $('.date_entered', elem).text().trim();
		let date = dateStr.match(/\d+\/\d+\/\d+/)[0].split('/');
		date = new Date(`${date[1]}/${date[0]}/${date[2]}`);
		date = new Date(date.getTime() + 43200000);
		box.date = date;
		let timeDiff = Math.abs(Date.now() - date.getTime());
		let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		if (maxAge && diffDays > maxAge) {
			requestCount--;
			return;
		}

		try {
			var $ = cheerio.load(await request('http://www.daft.ie/' + href));
		} catch (err) {
			throw err;
		}

		let warning = $('.warning');
		if (warning.length > 0 && !warning.hasClass('error_text')) {
			throw warning.text();
		}

		infoStr = $('#map_information .map_info_box').text().split(':');
		box.address = infoStr[1].split('Distance to')[0].trim();
		let match = box.address.match(/Dublin \d{1,2}W?/);
		if (match)
			match = 'D' + (match[0].split(' ')[1].length == 1 ? '0' : '') +
				match[0].split(' ')[1];
		box.areaCode = match || 'unknown';
		box.distance = +infoStr[2].split('km')[0].trim();

		box.images = $('.pbxl_carousel_item > img').map((_, el) => {
			return $(el).attr('src');
		}).get();

		let overviewStr = $('#overview ul').text().trim().split('\n');
		if (overviewStr.length < 2) {
			box.furnished = null;
			box.bedTypes = null;
		} else {
			box.furnishing = !!overviewStr[0].match(/\bFurnished/i);
			let bedTypes = overviewStr[1].split('(')[1].split(')')[0];
			box.bedTypes = {};
			bedTypes.split(',').forEach(b => {
				box.bedTypes[b.trim().split(' ')[1]] = +b.trim().split('')[0];
			});
		}

		box.facilities = [];
		$('#facilities li').each((_, f) =>box.facilities.push($(f).text()));

		box.url = $('.description_extras > a:nth-of-type(1)').attr('href');
		box.source = 'daft';

		boxes.push(box);
		return true;
	}

	function align(string, n) {
		return string + ' '.repeat(Math.max(n - ('' + string).length, 0));
	}

	let count = await countListings();

	let promises = [];
	for (let i = 0; i < count / 20 && i < limit; i++) {
		promises.push(scanPage(i));
	}
	await Promise.all(promises);
	return boxes;
};
