const unitLengths = {};
unitLengths.second = 1000;
unitLengths.minute = unitLengths.second * 60;
unitLengths.hour = unitLengths.minute * 60;
unitLengths.day = unitLengths.hour * 24;
unitLengths.week = unitLengths.day * 7;
unitLengths.month = unitLengths.day * 30;
unitLengths.year = unitLengths.day * 365;

module.exports.parse = (str) => {
	let words = str.replace('ago', '').trim().split(' ');
	let time = 0;

	if (('' + +words[0]) === words[0]) {
		let num = +words[0];
		let unit = words[1].replace(/s$/, '');
		time = num * unitLengths[unit];
	} else {
		if (words[0] == 'yesterday') {
			time = unitLengths.day;
			// TODO: Detect and adjust for "morning" "afternoon" etc.
		}
		if (words[0] == 'last') time = unitLengths[words[1]];
	}

	return new Date(Date.now() - time);
};
