const WebServer = require('./web');
const Scrapers = require('./scrapers');
const Listings = require('./listings');
const User = require('./user');
const JsonDB = require('node-json-db');

class Server {
	constructor() {
		let ids = ['col', 'mar'];
		for (let i = ids.length; i; i--) {
			let j = Math.floor(Math.random() * i);
			[ids[i - 1], ids[j]] = [ids[j], ids[i - 1]];
		}

		this.users = {};
		ids.forEach(id => this.users[id] = new User(id));

		this.webServer = new WebServer(this);
		this.scrapers = new Scrapers(this);
		this.listings = new Listings(this);

		this.db = new JsonDB("findahouse", true, true);

		process.on("unhandledRejection", function(reason, p) {
			console.log("Unhandled", reason, p);
			throw reason;
		});

		process.on('SIGINT', _ => {
			try {
				this.stop()
			} finally {
				process.exit();
			}
		});
	}

	stop() {
		console.log('Stopping'.bold);
		this.listings.save();
	}

	start() {
		this.webServer.start();
		setInterval(this.update.bind(this), 1000 * 3600 * 3);
		this.update()
	}

	async update() {
		this.listings.save();
		let d = new Date();
		console.log(`<${d.toTimeString().split(' ')[0]}> ` + 'Updating'.bold);
		await this.scrapers.run({
			maxAge: 10
		});
		console.log(`<${d.toTimeString().split(' ')[0]}> `
			+ ('Found ' + this.listings.items.size + ' listings').bold);
	}
}

global.server = new Server();
server.start();
